db.fruits.aggregate([
    { $match: { supplier: "Yellow Farms" } },
    { $match: { "price": { $lt: 50 } } },
    { $count: "totaItems" }
]
)

db.fruits.aggregate([
    { $match: { "price": { $lt: 30 } } },
    { $count: "totaItems" }
]
)

db.fruits.aggregate([
    { $match: { supplier: "Yellow Farms" } },
    {$group: {_id:"averagePrice", avgPrice:{$avg:"$price"}}}
]
)

db.fruits.aggregate([
    { $match: { supplier: "Red Farms Inc." } },
    {$group: {_id:"maxPrice", maxPrice:{$max:"$price"}}}
]
)

db.fruits.aggregate([
    { $match: { supplier: "Red Farms Inc." } },
    {$group: {_id:"minPrice", minPrice:{$min:"$price"}}}
]
)

